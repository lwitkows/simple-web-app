﻿#Simple Web App

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/08ca8906b7b340f6bba8ec50b5050fb2)](https://www.codacy.com/app/lwitkows/simple-web-app?utm_source=lwitkows@bitbucket.org&amp;utm_medium=referral&amp;utm_content=lwitkows/simple-web-app&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/08ca8906b7b340f6bba8ec50b5050fb2)](https://www.codacy.com/app/lwitkows/simple-web-app?utm_source=lwitkows@bitbucket.org&utm_medium=referral&utm_content=lwitkows/simple-web-app&utm_campaign=Badge_Coverage)

This is a simple Springboot web application to present some design decisions:  
 * clear separation of layers (controller, domain /entities & services/, exception handling, security)  
 * JWT-based authentication  
 * unit tests  
 * integration tests for REST Api (RestAssured, DBUnit)  
 * CI integrations: Bitbucket Pipelines, Codacy, PMD configs  
 * interactive Swagger 2 documentation (password protected)  
 
 Technologies/libraries:  
  * Architecture: Springboot 2.0 / Spring 5.0 / Spring REST  
  * Persistence: EclipseLink + Spring Data + MySQL  
  * Authentication/Authorization: Spring Security + JWT  
  * Testing: JUnit, Mockito, Hamcrest, RestAssured, H2 DB  
  * Build: Gradle  

##Getting Started

### Prerequisites
Mysql database (preferably on http://localhost:3306 with user 'root' and password 'root')  

### Installing
Create 'simple-web-app' database  
If necessary update database settings (url, user, password) in 'src/main/resources/application.properties'   
Init database using one of these commands:  
`./gradlew db_init` to create all necessary tables  
`./gradlew db_test_init --stacktrace` to drop and create all tables and load test data  

### Suggested IntelliJ plugins
Markdown support (readme.md renderer)  
PMDPlugin for static code analysis

## Building and running

### Rebulding/running app
`./gradlew bootRun`  

### Running the tests
`./gradlew check`  

### Building fat jar
`./gradlew bootJar`  
JAR is in in ./build/libs folder  

### Sending coverage data to Codacy
`./gradlew sendCoverageToCodacy -PprojectToken=CODACY_PROJECT_TOKEN`  

### Api documentation (Swagger 2) 
[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)  
Those endpoints are password protected: user=swagger password=szwagier  
To use API as regular user, first use '/login' to get JWT token, then enter "Bearer [JWTToken here]" in 'Authorize' popup  

## Configuration

###Where to find server port/database settings:
`src/main/resources/application.properties`  
