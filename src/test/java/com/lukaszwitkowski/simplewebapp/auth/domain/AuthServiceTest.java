package com.lukaszwitkowski.simplewebapp.auth.domain;

import com.lukaszwitkowski.simplewebapp.user.domain.User;
import com.lukaszwitkowski.simplewebapp.user.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by l.witkowski on 02.01.2018
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthServiceTest {
    private static final String TEST_EMAIL_1 = "one@two.three";
    private static final String TEST_EMAIL_2 = "two@two.three";
    private static final String TEST_PASSWORD = "123";
    private static final String TEST_PASSWORD_HASH = "$2a$10$U2ialbInlZGMqpDZEpvpyeBHQbNyJEHdWA6mkd1c5QYOKXsGn3DVq";

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AuthService authService;
    
    private User user;

    @Before
    public void setUp() {
        user = new User(TEST_EMAIL_1, TEST_PASSWORD_HASH);
        user.setId(1l);
        
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        when(userRepository.findOneByEmail(TEST_EMAIL_1)).thenReturn(user);

        authService.setJwtSecret("123");
    }

    @Test
    public void testRegister() {
        User user2 = authService.registerUser(TEST_EMAIL_2, TEST_PASSWORD);
        assertThat(user2.getEmail(), is(TEST_EMAIL_2));
    }

    @Test(expected = EmailAlreadyRegisteredException.class)
    public void testRegisterEmailAlreadyRegistered() {
        authService.registerUser(TEST_EMAIL_1, TEST_PASSWORD);
    }

    @Test
    public void testLogin() {
        Session session = authService.login(TEST_EMAIL_1, TEST_PASSWORD);
        assertThat(session, is(not(nullValue())));
    }

    @Test(expected = InvalidLoginCredentialsException.class)
    public void testLoginInvalidUsername() {
        authService.login(TEST_EMAIL_2, TEST_PASSWORD);
    }

    @Test(expected = InvalidLoginCredentialsException.class)
    public void testLoginInvalidPassword() {
        authService.login(TEST_EMAIL_1, TEST_PASSWORD + "_1");
    }

    @Test
    public void testLogout() {
        Session session = authService.createSession(user, Instant.now().plusSeconds(5));
        authService.logout(session);
        
        assertThat(authService.getValidSession(session.getToken()), is(nullValue()));
    }

    @Test
    public void testGetValidSession() {
        Session session = authService.createSession(user, Instant.now().plusSeconds(5));

        Session session2 = authService.getValidSession(session.getToken());
        assertThat(session2, is(not(nullValue())));
    }

    @Test
    public void testGetValidSessionExpired() {
        Session session = authService.createSession(user, Instant.now().minusSeconds(5));

        Session session2 = authService.getValidSession(session.getToken());
        assertThat(session2, is(nullValue()));
    }
}