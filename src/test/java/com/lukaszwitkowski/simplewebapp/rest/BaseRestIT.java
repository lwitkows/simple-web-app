package com.lukaszwitkowski.simplewebapp.rest;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.google.common.net.HttpHeaders;
import com.lukaszwitkowski.simplewebapp.Application;
import com.lukaszwitkowski.simplewebapp.auth.domain.AuthService;
import com.lukaszwitkowski.simplewebapp.auth.domain.Session;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static io.restassured.RestAssured.given;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * Created by l.witkowski on 02.01.2018
 */
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@ActiveProfiles("test")
public class BaseRestIT {
    @Autowired
    private AuthService authService;

    public BaseRestIT() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails(LogDetail.ALL);
    }

    @LocalServerPort
    public void setLocalPort(int localPort) {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .addHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_UTF8_VALUE)
                .setPort(localPort)
                .build();
    }

    protected RequestSpecification asUser(String email) {
        return given().
                filter((requestSpec, responseSpec, ctx) -> {
                    Session session = authService.login(email, "123");
                    requestSpec.header(HttpHeaders.AUTHORIZATION, "Bearer " + session.getToken());
                    return ctx.next(requestSpec, responseSpec);
                });
    }
}