package com.lukaszwitkowski.simplewebapp.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

/**
 * Created by l.witkowski on 02.01.2018
 */
@RunWith(SpringRunner.class)
public class CorsRestIT extends BaseRestIT {

    @Test
    public void testPreflightRequest() {
        given()
                .header(HttpHeaders.ORIGIN, "http://lukaszwitkowski.com")
                .header(HttpHeaders.ACCESS_CONTROL_REQUEST_METHOD, "GET")
                .options("/api/version")
                .then()
                .statusCode(HttpStatus.OK.value())
                .header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, is("http://lukaszwitkowski.com"))
                .header(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, is(not(nullValue())));
    }
}