package com.lukaszwitkowski.simplewebapp.rest;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

/**
 * Created by l.witkowski on 02.01.2018
 */
@RunWith(SpringRunner.class)
@DatabaseSetup(value = {"/datasets/_setup.xml"})
@DatabaseTearDown("/datasets/_teardown.xml")
public class UserControllerRestIT extends BaseRestIT {

    public static final String EMAIL = "contact@lukaszwitkowski.com";

    @Test
    public void testGetCurrentUser() {
        asUser(EMAIL)
                .get("/api/session/current_user")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("email", is("contact@lukaszwitkowski.com"));
    }

    @Test
    public void testGetUserById() {
        asUser(EMAIL)
                .get("/api/user/2")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("email", is("test_user@lukaszwitkowski.com"));
    }

    @Test
    public void testGetUserByIdNotFound() {
        asUser(EMAIL)
                .get("/api/user/3")
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }


    @Test
    public void testGetUserByIdUnauthorized() {
        given()
                .get("/api/user/2")
                .then()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }

    @Test
    public void testGetAllUsers() {
        asUser(EMAIL)
                .get("/api/user/?page=0&size=10")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("number", is(0))
                .body("size", is(10))
                .body("totalPages", is(1))
                .body("totalElements", is(2))
                .body("content[0].id", is(1))
                .body("content[0].email", is("contact@lukaszwitkowski.com"));
    }

    @Test
    public void testGetAllUsersUnauthorized() {
        given()
                .get("/api/user/?page=0&size=10")
                .then()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }
}