package com.lukaszwitkowski.simplewebapp.rest;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

/**
 * Created by l.witkowski on 02.01.2018
 */
@RunWith(SpringRunner.class)
@DatabaseSetup(value = {"/datasets/_setup.xml"})
@DatabaseTearDown("/datasets/_teardown.xml")
public class AuthControllerRestIT extends BaseRestIT {

    public static final String EMAIL = "contact@lukaszwitkowski.com";

    @Test
    public void testLoginOK() throws JSONException {
        given()
                .body(new JSONObject()
                        .put("email", EMAIL)
                        .put("password", "123")
                        .toString())
                .post("/api/auth/login")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("accessToken", is(not(nullValue())))
                .body("user.email", is(EMAIL));
    }

    @Test
    public void testLogoutOK() {
        asUser("contact@lukaszwitkowski.com")
                .post("/api/auth/logout")
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void testLoginInvalidLoginUnauthorized() throws JSONException {
        given()
                .body(new JSONObject()
                        .put("email", "1_contact@lukaszwitkowski.com")
                        .put("password", "123")
                        .toString())
                .post("/api/auth/login")
                .then()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }

    @Test
    public void testLoginInvalidPasswordUnauthorized() throws JSONException {
        given()
                .body(new JSONObject()
                        .put("email", EMAIL)
                        .put("password", "123456")
                        .toString())
                .post("/api/auth/login")
                .then()
                .statusCode(HttpStatus.SC_UNAUTHORIZED);
    }

    @Test
    public void testLoginMissingPasswordBadRequest() throws JSONException {
        given()
                .body(new JSONObject()
                        .put("email", EMAIL)
                        .put("password", null)
                        .toString())
                .post("/api/auth/login")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void testRegisterOK() throws JSONException {
        given()
                .body(new JSONObject()
                        .put("email", EMAIL + "2")
                        .put("password", "123")
                        .toString())
                .post("/api/auth/register")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("email", is(EMAIL + "2"));

        given()
                .body(new JSONObject()
                        .put("email", EMAIL + "2")
                        .put("password", "123")
                        .toString())
                .post("/api/auth/login")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("accessToken", is(not(nullValue())))
                .body("user.email", is(EMAIL + "2"));
    }

    @Test
    public void testRegisterDuplicateEmailConflict() throws JSONException {
        given()
                .body(new JSONObject()
                        .put("email", EMAIL)
                        .put("password", "123")
                        .toString())
                .post("/api/auth/register")
                .then()
                .statusCode(HttpStatus.SC_CONFLICT)
                .body("message", is("email_already_registered"));
    }
}