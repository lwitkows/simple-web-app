CREATE TABLE IF NOT EXISTS user (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  email varchar(190) DEFAULT NULL,
  password_hash varchar(255) DEFAULT NULL,
  jwt_version bigint(20) NOT NULL DEFAULT 1,
  PRIMARY KEY (id),
  UNIQUE INDEX email_UNIQUE (email ASC)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;