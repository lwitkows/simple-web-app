package com.lukaszwitkowski.simplewebapp.auth.domain;

import com.lukaszwitkowski.simplewebapp.user.domain.User;

import java.time.Instant;

/**
 * Created by lwitkowski on 27.01.2018.
 */
public class Session {
    private User user;
    private String token;
    private Instant expirationDate;

    public Session(User user, String token, Instant expirationDate) {
        this.user = user;
        this.token = token;
        this.expirationDate = expirationDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public Instant getExpirationDate() {
        return expirationDate;
    }
}
