package com.lukaszwitkowski.simplewebapp.auth.domain;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by lwitkowski on 11.02.2018.
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class InvalidSessionException extends RuntimeException {
    public InvalidSessionException() {
        super("invalid_session");
    }
}
