package com.lukaszwitkowski.simplewebapp.auth.domain;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by lwitkowski on 27.01.2018.
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class EmailAlreadyRegisteredException extends RuntimeException {
    private final String email;

    public EmailAlreadyRegisteredException(String email) {
        super("email_already_registered");
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
