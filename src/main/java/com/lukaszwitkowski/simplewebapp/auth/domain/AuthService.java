package com.lukaszwitkowski.simplewebapp.auth.domain;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.lukaszwitkowski.simplewebapp.user.domain.User;
import com.lukaszwitkowski.simplewebapp.user.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Date;

/**
 * Created by l.witkowski on 05.01.2018
 */
@Service
public class AuthService {

    private static final String JWT_TOKEN_VERSION = "tkv";
    private static final String JWT_ISSUER = "com.lukaszwitkowski.simplewebapp";

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthService.class);
    
    @Value("${auth.session.lifetime.seconds}")
    private long sessionLifetimeSeconds = 60 * 60;

    private UserRepository userRepository;
    private Algorithm algorithm;
    private JWTVerifier verifier;

    @Autowired
    public AuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
    @Transactional
    public User registerUser(String email, String password) {
        if (isEmailUsed(email)) {
            throw new EmailAlreadyRegisteredException(email);
        }
        String passwordHash = generatePasswordHash(password);
        User user = new User(email, passwordHash);
        userRepository.save(user);
        return user;
    }

    public Session login(String email, String password) {
        User user = userRepository.findOneByEmail(email);
        if (user == null) {
            throw new InvalidLoginCredentialsException();
        }

        if (!checkPassword(user, password)) {
            throw new InvalidLoginCredentialsException();
        }
        return createSession(user, Instant.now().plusSeconds(this.sessionLifetimeSeconds));
    }

    public void logout(Session session) {
        if (session != null) {
            session.getUser().nextJwtVersion();
            userRepository.save(session.getUser());
        }
    }

    public Session getValidSession(String token) {
        if (token == null) {
            return null;
        }
        DecodedJWT jwt;
        try {
            jwt = verifier.verify(token);
        } catch (JWTVerificationException exception) {
            LOGGER.debug("JWT verification failed", exception);
            return null;
        }

        Long userId = Long.valueOf(jwt.getSubject());
        User user = userRepository.findById(userId).get();
        Long jwtVersion = jwt.getClaim(JWT_TOKEN_VERSION).asLong();
        if (jwtVersion == null || user.getJwtVersion() != jwtVersion) {
            LOGGER.debug("JWT version invalid, expected: {}, found: {}", user.getJwtVersion(), 
                    jwtVersion == null ? "null" : jwtVersion);
            return null;
        }
        return new Session(user, jwt.getToken(), jwt.getExpiresAt().toInstant());
    }

    @Transactional
    public Session createSession(User user, Instant expiresAt) {
        long jwtVersion = user.nextJwtVersion();
        userRepository.save(user);

        try {
            String token = JWT.create()
                    .withIssuer(JWT_ISSUER)
                    .withClaim(JWT_TOKEN_VERSION, jwtVersion)
                    .withSubject(user.getId().toString())
                    .withExpiresAt(Date.from(expiresAt))
                    .sign(this.algorithm);

            return new Session(user, token, expiresAt);
        } catch (JWTCreationException exception) {
            LOGGER.debug("createSession failed", exception);
        }
        return null;
    }

    @Value("${auth.session.secret}")
    public void setJwtSecret(String secret) {
        this.algorithm = Algorithm.HMAC256(secret.getBytes());
        this.verifier = JWT.require(this.algorithm)
                .withIssuer(JWT_ISSUER)
                .build();
    }

    private boolean isEmailUsed(String email) {
        return userRepository.findOneByEmail(email) != null;
    }

    private String generatePasswordHash(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    private boolean checkPassword(User user, String password) {
        try {
            return BCrypt.checkpw(password, user.getPasswordHash());
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}