package com.lukaszwitkowski.simplewebapp.auth.rest;

import javax.validation.constraints.NotNull;

/**
 * Created by lwitkowski on 27.01.2018.
 */
public class RegisterUserRequest {

    @NotNull
    private String email;

    @NotNull
    private String password;

    public RegisterUserRequest() {}

    public RegisterUserRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
