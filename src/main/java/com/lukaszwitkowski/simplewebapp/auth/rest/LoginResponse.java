package com.lukaszwitkowski.simplewebapp.auth.rest;

import com.lukaszwitkowski.simplewebapp.auth.domain.Session;
import com.lukaszwitkowski.simplewebapp.user.rest.UserTO;

/**
 * Created by lwitkowski on 27.01.2018.
 */
public class LoginResponse {
    private UserTO user;
    private String accessToken;

    public LoginResponse(Session session) {
        this.user = new UserTO(session.getUser());
        this.accessToken = session.getToken();
    }

    public UserTO getUser() {
        return user;
    }
    
    public String getAccessToken() {
        return accessToken;
    }
}
