package com.lukaszwitkowski.simplewebapp.auth.rest;

import com.lukaszwitkowski.simplewebapp.auth.domain.AuthService;
import com.lukaszwitkowski.simplewebapp.auth.domain.Session;
import com.lukaszwitkowski.simplewebapp.config.SwaggerConfig;
import com.lukaszwitkowski.simplewebapp.user.rest.UserTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by l.witkowski on 26.01.2018
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ApiOperation("Registers new user and returns it")
    @ApiResponses({
            @ApiResponse(code = 200, response = UserTO.class, message = "OK"),
            @ApiResponse(code = 409, message = "Email already registered")
    })
    public UserTO register(@RequestBody @Valid RegisterUserRequest registerUserRequest) {
        return new UserTO(
                authService.registerUser(registerUserRequest.getEmail(), registerUserRequest.getPassword())
        );
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ApiOperation("Authenticates user and returns user and JWT session token")
    @ApiResponses({
            @ApiResponse(code = 200, response = LoginResponse.class, message = "OK"),
            @ApiResponse(code = 401, message = "Invalid login credentials")
    })
    public LoginResponse login(@RequestBody @Valid LoginRequest loginRequest) {
        return new LoginResponse(
                authService.login(loginRequest.getEmail(), loginRequest.getPassword())
        );
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ApiOperation(value = "Terminates user session", authorizations = {@Authorization(SwaggerConfig.AUTH_USER)})
    public void logout(Session session) {
        authService.logout(session);
    }
}