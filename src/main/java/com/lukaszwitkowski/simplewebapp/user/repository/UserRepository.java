package com.lukaszwitkowski.simplewebapp.user.repository;

import com.lukaszwitkowski.simplewebapp.user.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by lwitkowski on 27.01.2018.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findOneByEmail(String email);
}
