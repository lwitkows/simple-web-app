package com.lukaszwitkowski.simplewebapp.user.domain;

import javax.persistence.*;

/**
 * Created by lwitkowski on 27.01.2018.
 */
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "password_hash")
    private String passwordHash;

    @Column(name = "jwt_version")
    private long jwtVersion = 1;
    
    public User() {
    }

    public User(String email, String passwordHash) {
        this.email = email;
        this.passwordHash = passwordHash;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public long getJwtVersion() {
        return jwtVersion;
    }

    public void setJwtVersion(long jwtVersion) {
        this.jwtVersion = jwtVersion;
    }

    public long nextJwtVersion() {
        return ++this.jwtVersion;
    }
}
