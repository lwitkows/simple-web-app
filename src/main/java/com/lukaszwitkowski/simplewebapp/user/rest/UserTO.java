package com.lukaszwitkowski.simplewebapp.user.rest;

import com.lukaszwitkowski.simplewebapp.user.domain.User;

/**
 * Created by lwitkowski on 27.01.2018.
 */
public class UserTO {

    private Long id;
    private String email;

    public UserTO(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }
}
