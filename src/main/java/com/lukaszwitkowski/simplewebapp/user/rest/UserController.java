package com.lukaszwitkowski.simplewebapp.user.rest;

import com.lukaszwitkowski.simplewebapp.auth.domain.Session;
import com.lukaszwitkowski.simplewebapp.config.SwaggerConfig;
import com.lukaszwitkowski.simplewebapp.user.domain.User;
import com.lukaszwitkowski.simplewebapp.user.repository.UserRepository;
import com.lukaszwitkowski.simplewebapp.util.NotFoundException;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by l.witkowski on 05.01.2018
 */
@RestController
@RequestMapping("/api")
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/session/current_user", method = RequestMethod.GET)
    @ApiOperation(value = "Returns currently logged in user", authorizations = {@Authorization(SwaggerConfig.AUTH_USER)})
    public UserTO getSessionUser(Session session) {
        return new UserTO(session.getUser());
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Returns user by id", authorizations = {@Authorization(SwaggerConfig.AUTH_USER)})
    @ApiImplicitParam(value = "User ID", name = "id")
    @ApiResponses({
            @ApiResponse(code = 200, response = UserTO.class, message = "OK"),
            @ApiResponse(code = 404, message = "User not found")
    })
    public ResponseEntity getUserById(@PathVariable("id") User user) {
        if (user == null) {
            throw new NotFoundException("User not found");
        }
        return ResponseEntity.ok(new UserTO(user));
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @ApiOperation(value = "Returns pageable list of users", authorizations = {@Authorization(SwaggerConfig.AUTH_USER)})
    public Page<UserTO> getAll(@PageableDefault Pageable pageable) {
        return userRepository.findAll(pageable).map(UserTO::new);
    }
}