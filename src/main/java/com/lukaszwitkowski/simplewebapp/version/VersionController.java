package com.lukaszwitkowski.simplewebapp.version;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by l.witkowski on 05.01.2018
 */
@RestController
public class VersionController {
    @Value("${spring.application.name}")
    private String appName;

    @Value("${app.build.version}")
    private String appBuildVersion;

    @Value("${app.build.revision}")
    private String appBuildRevision;

    @Value("${app.build.revisionDate}")
    private String appBuildRevisionDate;

    @Value("${app.build.date}")
    private String appBuildDate;

    @RequestMapping(value = "/api/version", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation("Returns app name, version, git revision and build date")
    public VersionResponse getVersion() {
        return new VersionResponse(appName, appBuildVersion, appBuildRevision, appBuildRevisionDate, appBuildDate);
    }
}