package com.lukaszwitkowski.simplewebapp.version;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by lwitkowski on 27.01.2018.
 */
public class VersionResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String application;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String version;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String revision;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String revisionDate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String buildDate;

    public VersionResponse(String applicationName, String version, String revision, String revisionDate, String buildDate) {
        this.application = applicationName;
        this.version = version;
        this.revision = revision;
        this.revisionDate = revisionDate;
        this.buildDate = buildDate;
    }

    public String getApplication() {
        return application;
    }

    public String getVersion() {
        return version;
    }

    public String getRevision() {
        return revision;
    }

    public String getBuildDate() {
        return buildDate;
    }

    public String getRevisionDate() {
        return revisionDate;
    }
}