package com.lukaszwitkowski.simplewebapp.util;

import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by l.witkowski on 14.02.2018
 */
public class LoggingFilter extends OncePerRequestFilter implements Ordered {
    private static final Logger LOGGER = LoggerFactory.getLogger("API");
    private static final String REQUEST_ATTR_START_TIME = "LoggingFilter.startTime";

    private Set<String> serializableMimeTypes = Sets.newHashSet();

    @Value("${api.logging.payload.mimetypes:#{null}}")
    public void setPayloadMimeTypes(String mimeTypes) {
        if (mimeTypes == null) {
            this.serializableMimeTypes = Sets.newHashSet();
        } else {
            this.serializableMimeTypes = Arrays.asList(mimeTypes.split(",")).stream().map(m -> m.toLowerCase()).collect(Collectors.toSet());
        }
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    @Override
    protected boolean shouldNotFilterAsyncDispatch() {
        return false;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        if(!LOGGER.isDebugEnabled()) {
            chain.doFilter(req, res);
            return;
        }


        HttpServletRequest request = req;
        HttpServletResponse response = res;
        boolean firstRun = !isAsyncDispatch(request);
        if(firstRun) {
            request.setAttribute(REQUEST_ATTR_START_TIME, System.currentTimeMillis());
            if (LOGGER.isTraceEnabled()) {
                request = new ContentCachingRequestWrapper(request);
                response = new ContentCachingResponseWrapper(response);
            }
        }

        try {
            chain.doFilter(request, response);
        } finally {
            if(!request.isAsyncStarted() || !firstRun) {
                traceAndFinalize(request, response, !firstRun);
            }
        }
    }

    private void traceAndFinalize(HttpServletRequest request, HttpServletResponse response, boolean async) throws IOException {
        logSummary(request, response, async);
        logPayloads(request, response);
        if (LOGGER.isTraceEnabled()) {
            ((ContentCachingResponseWrapper) response).copyBodyToResponse();
        }
    }

    private void logSummary(HttpServletRequest request, HttpServletResponse response, boolean async) {
        if (!LOGGER.isDebugEnabled()) {
            return;
        }
        StringBuilder builder = new StringBuilder(100);
        builder.append(request.getMethod())
                .append(' ')
                .append(request.getRequestURI());

        if (request.getQueryString() != null) {
            builder.append('?')
                    .append(request.getQueryString());
        }

        long startTime = (long) request.getAttribute(REQUEST_ATTR_START_TIME);
        long executionTime = System.currentTimeMillis() - startTime;

        builder.append(" returned ")
                .append(response.getStatus())
                .append(" (")
                .append(HttpStatus.valueOf(response.getStatus()).getReasonPhrase())
                .append(") , processed in ")
                .append(executionTime)
                .append(" ms");

        if (async) {
            builder.append(" [async] ");
        }

        LOGGER.debug(builder.toString());
    }

    private void logPayloads(HttpServletRequest request, HttpServletResponse response) {
        if (!LOGGER.isTraceEnabled()) {
            return;
        }
        String requestBody = serializePayload((ContentCachingRequestWrapper) request);
        if (!requestBody.isEmpty()) {
            LOGGER.trace("Request: {}", requestBody);
        }

        String responseBody = serializePayload((ContentCachingResponseWrapper) response);
        if (!responseBody.isEmpty()) {
            LOGGER.trace("Response: {}", responseBody);
        }
    }

    private String serializePayload(ContentCachingRequestWrapper request) {
        return serializePayload(request.getContentAsByteArray(), request.getCharacterEncoding(),
                request.getContentType() == null ? MediaType.APPLICATION_JSON_UTF8_VALUE : request.getContentType());
    }

    private String serializePayload(ContentCachingResponseWrapper response) {
        return serializePayload(response.getContentAsByteArray(), response.getCharacterEncoding(), response.getContentType());
    }

    private String serializePayload(byte[] buf, String encoding, String contentType) {
        if (contentType == null) {
            return "";
        }

        if (!serializableMimeTypes.contains(contentType.toLowerCase())) {
            return "<" +contentType+ ">";
        }
        if (buf.length > 0) {
            int length = Math.min(buf.length, 5120);
            try {
                return new String(buf, 0, length, encoding);
            } catch (UnsupportedEncodingException ex) {
                return "<unsupported encoding: " +encoding+ ", contentType: "+contentType+">";
            }
        }
        return "";
    }
}
