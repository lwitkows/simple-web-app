package com.lukaszwitkowski.simplewebapp.config.security;

import com.google.common.collect.Lists;
import com.lukaszwitkowski.simplewebapp.auth.domain.AuthService;
import com.lukaszwitkowski.simplewebapp.auth.domain.Session;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by l.witkowski on 26.03.2018
 */
public class JwtAuthTokenFilter extends OncePerRequestFilter {

    private final AuthService authService;

    public JwtAuthTokenFilter(AuthService authService) {
        this.authService = authService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) 
            throws ServletException, IOException {
        String header = request.getHeader("Authorization");
        if (header != null && header.startsWith("Bearer ")) {
            Session session = authService.getValidSession(header.substring(7));
            if (session == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
            SecurityContextHolder.getContext().setAuthentication(sessionToAuthentication(session));
        }
        chain.doFilter(request, response);
    }

    private Authentication sessionToAuthentication(Session session) {
        return new PreAuthenticatedAuthenticationToken(
                session,
                session.getToken(),
                Lists.newArrayList()
        );
    }
}