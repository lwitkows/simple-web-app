package com.lukaszwitkowski.simplewebapp.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by l.witkowski on 20.03.2018
 */
@Configuration
@Order(2)
public class SwaggerSecurityConfig extends WebSecurityConfigurerAdapter {
    private static final String SWAGGER_USERNAME = "swagger";
    private static final String SWAGGER_PASSWORD = "{bcrypt}$2a$10$aH6vFy.aCCBWqAsCb248uesDwSUlglINUMwqz0QkZf2BtHNn19f1y";
    private static final String SWAGGER_ACCESS = "SWAGGER_ACCESS";

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser(SWAGGER_USERNAME)
                .password(SWAGGER_PASSWORD)
                .authorities(SWAGGER_ACCESS);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
            .antMatcher("/v2/api-docs")
            .httpBasic().and()
            .authorizeRequests().anyRequest().hasAuthority(SWAGGER_ACCESS);
        // @formatter:on
    }
}




   

    

    