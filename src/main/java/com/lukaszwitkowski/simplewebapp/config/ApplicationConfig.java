package com.lukaszwitkowski.simplewebapp.config;

import com.google.common.collect.ImmutableList;
import com.lukaszwitkowski.simplewebapp.auth.rest.SessionArgumentResolver;
import com.lukaszwitkowski.simplewebapp.util.LoggingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableJpaRepositories("com.lukaszwitkowski.simplewebapp")
@EnableTransactionManagement
@PropertySource("classpath:version.properties")
@EnableScheduling
public class ApplicationConfig implements WebMvcConfigurer {

    @Autowired
    private SessionArgumentResolver sessionArgumentResolver;

    @Value("${api.cors.allowed.origins}")
    private String corsAllowedOrigins;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(sessionArgumentResolver);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        if (corsAllowedOrigins != null) {
            registry.addMapping("/**")
                    .allowedOrigins(corsAllowedOrigins.split(","))
                    .allowedMethods("GET", "POST", "PUT");
        }
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList(corsAllowedOrigins.split(",")));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(ImmutableList.of("Authorization", "Cache-Control", "Content-Type"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
    
    @Bean
    public LoggingFilter loggingFilter() {
        return new LoggingFilter();
    }
}
