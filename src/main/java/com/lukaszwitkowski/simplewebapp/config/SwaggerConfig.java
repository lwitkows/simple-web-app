package com.lukaszwitkowski.simplewebapp.config;

import com.fasterxml.classmate.TypeResolver;
import com.lukaszwitkowski.simplewebapp.auth.domain.Session;
import com.lukaszwitkowski.simplewebapp.user.domain.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.*;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;
import java.util.Arrays;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

@Configuration
@EnableSwagger2
@EnableWebSecurity
public class SwaggerConfig extends WebSecurityConfigurerAdapter {
    public static final String AUTH_USER = "User";

    @Bean
    public Docket api(final TypeResolver resolver) {
        return swagger(resolver)
                .apiInfo(new ApiInfoBuilder()
                        .title("Simple-web-app Api Documentation")
                        .version("1.0")
                        .build()
                )
                .ignoredParameterTypes(User.class, Session.class);
    }

    @Bean
    public UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                .displayRequestDuration(true)
                .validatorUrl(null)
                .build();
    }

    private Docket swagger(final TypeResolver resolver) {
        return new Docket(DocumentationType.SWAGGER_2)
                .securitySchemes(Arrays.asList(
                        new ApiKey(AUTH_USER, "Authorization", "header")
                ))
                .ignoredParameterTypes(HttpServletRequest.class, HttpServletResponse.class)
                .genericModelSubstitutes(ResponseEntity.class, DeferredResult.class)
                .alternateTypeRules(
                        newRule(resolver.resolve(Pageable.class), resolver.resolve(pageableMixin()))
                )
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(PathSelectors.any())
                .build();
    }

    private Type pageableMixin() {
        return new AlternateTypeBuilder()
                .fullyQualifiedClassName(
                        String.format("%s.generated.%s",
                                Pageable.class.getPackage().getName(),
                                Pageable.class.getSimpleName()))
                .withProperties(newArrayList(
                        property(Integer.class, "page"),
                        property(Integer.class, "size"),
                        property(String.class, "sort")
                ))
                .build();
    }

    private AlternateTypePropertyBuilder property(Class<?> type, String name) {
        return new AlternateTypePropertyBuilder()
                .withName(name)
                .withType(type)
                .withCanRead(true)
                .withCanWrite(true);
    }
}